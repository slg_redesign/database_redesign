README for database_redesign:
=============================
Author(s): Jack Wang, Greg Cho, Drew Foland
Company: Super League Gaming
Last Update: September 12th, 2017


Overview: 
=========
Redesigning the existing database schema. All tables added to either create.sql or redesign.py will be recorded here. The relational model is maintained in the new_nthgames folder while the old model is in current_nthgames. Note that as of September 4th, due to the overall changes that need to occur to successfully implement this new database schema, this project is now considered an overhall/upgrade to the current application platform.


Usage:
======
To run the sql file, while in the mysql command line (or from any command line for that matter) type:

mysql (db_name) -u (username) -h (host) -password=(password) < create.sql

Where create.sql is located in the redesign folder.


Important Tables:
=================

users: 
Keeps track of user information.
Primary Key(s): user_id


cities: 
Contains city information.
Primary Key(s): city_id


countries: 
Contains country information.
Primary Key(s): country_id


states: 
Contains state information.
Primary Key(s): state_id


clubs: 
Keeps track of club information(?)
Primary Key(s): club_id
Foreign Key(s): (city_id) references cities(city_id), (state_id) references states(state_id), (country_id) references countries(country_id)


seasons: 
Tracks the seasons (parent for the entire tournament structure).
Primary Key(s): season_id
Foreign Key(s): 


series: 
Tracks series (substructure of a season).
Primary Key(s): series_id
Foreign Key(s):


events: 
Keeps track of all events (substructure of a series).
Primary Key(s): event_id
Foreign Key(s):


matches: 
Match information (substructure of an event).
Primary Key(s): match_id
Foreign Key(s):


lol_match_stats: 
Keeps track of lol match statistics (spritual substructure of matches).
Primary Key(s): user_id, match_id
Foreign Key(s):


match_participants: 
Keeps track of match participants. It's really just a simple relational table.
Primary Key(s): user_id, match_id
Foreign Key(s):


ticketing: 
Handles all general ticket information (prevents redundant data storage).
Primary Key(s): ticketing_id
Foreign Key(s): (event_id) references events(event_id)

tickets: 
Handles ticket specific information.
Primary Key(s): ticket_id
Foreign Key(s): (user_id) references users(user_id), (ticketing_id) references ticketing(ticketing_id)


user_payment_info: 
Stores user payment info.
Primary Key(s): payment_info_id
Foreign Key(s): (user_id) references users(user_id)


user_ticket_transactions: 
Stores all user ticket transactions.
Primary Key(s): payment_info_id, ticket_id
Foreign Key(s): (payment_info_id) references user_payment_info(payment_info_id), (ticket_id) references tickets(ticket_id)


user_event_cart: 
Stores user cart information.
Primary Key(s): user_id, event_id
Foreign Key(s): (user_id) references users(user_id), (event_id) references events(event_id)


club_teams:
Mostly just to ensure referential integrity. Note that the club teams do not contain a reference to users since teams are not always set. The user is related to teams in the club roster as it appears to be more rational to use the user as a condidate key for which team he/she belongs to than to have the team be the candidate key.
Primary Key(s): team_id
Foreign Key(s): club_id references clubs(club_id)
