DROP DATABASE IF EXISTS nthgames2;

CREATE DATABASE IF NOT EXISTS nthgames2;

USE nthgames2;

CREATE TABLE IF NOT EXISTS cities (
    city_id int(11) AUTO_INCREMENT PRIMARY KEY,
    city varchar(255)
);

CREATE TABLE IF NOT EXISTS countries (
    country_id int(11) AUTO_INCREMENT PRIMARY KEY,
    country varchar(255)
);

CREATE TABLE IF NOT EXISTS states (
    state_id int(11) AUTO_INCREMENT PRIMARY KEY,
    state varchar(255)
);

CREATE TABLE IF NOT EXISTS users (
    user_id int(11) AUTO_INCREMENT PRIMARY KEY,
    open_id varchar(100) UNIQUE,
    password varchar(128) NOT NULL,
    last_login datetime,
    email varchar(255) NOT NULL UNIQUE,
    is_active BOOLEAN NOT NULL DEFAULT 0,
    is_staff BOOLEAN NOT NULL DEFAULT 0,
    phone_number varchar(100),
    date_created datetime DEFAULT CURRENT_TIMESTAMP,
    is_verified BOOLEAN NOT NULL DEFAULT 0,
    email_token varchar(64), -- for user verification
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL,
    is_superuser BOOLEAN NOT NULL DEFAULT 0,
    image varchar(100),
    jersey_size varchar(16),
    state_id int(11),
    address longtext,
    city_id int(11),
    zip_code varchar(20),
    date_modified datetime,
    minecraft_uuid varchar(255) ,
    slg_profile_name varchar(120),
    affiliate_id varchar(128),
    country_id int(11),
    laptop_status varchar(100),
    promo_code varchar(255),
    promo_credit_balance decimal(9,2),
    date_i_agree_policy_tos datetime,
    date_of_birth date,
    is_super_action_squad BOOLEAN NOT NULL DEFAULT 0,
    phone_token varchar(64), -- for phone verification
    phone_verified BOOLEAN NOT NULL DEFAULT 0,
    is_fill_player BOOLEAN NOT NULL DEFAULT 0,
    contact_preference varchar(50),
    newsletter_subscribe BOOLEAN NOT NULL DEFAULT 0,
    summoner_id varchar(20),
    summoner_name varchar(100),
    FOREIGN KEY (city_id) REFERENCES cities(city_id),
    FOREIGN KEY (state_id) REFERENCES states(state_id),
    FOREIGN KEY (country_id) REFERENCES countries(country_id)
);

CREATE TABLE IF NOT EXISTS user_payment_info (
    payment_info_id int(11) AUTO_INCREMENT PRIMARY KEY,
    user_id int(11),
    stripe_id VARCHAR(255), -- what is this?
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    card_fingerprint VARCHAR(255), -- lol wut?
    card_last_four int(4),
    card_kind VARCHAR(50),
    valid BOOLEAN,
    billing_email VARCHAR(255),
    name_on_card VARCHAR(255),
    billing_city_id int(11),
    billing_state_id int(11),
    billing_address VARCHAR(255),
    billing_zip VARCHAR(255),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (billing_city_id) REFERENCES cities(city_id),
    FOREIGN KEY (billing_state_id) REFERENCES states(state_id)
);

CREATE TABLE IF NOT EXISTS stripe_customer (
    user_id int(11),
    customer_id varchar(255),
    PRIMARY KEY (user_id, customer_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id)
);

CREATE TABLE IF NOT EXISTS games (
    game_id int(11) PRIMARY KEY AUTO_INCREMENT,
    game_name varchar(1000),
    ip_owner varchar(1000),
    CONSTRAINT game_name_req UNIQUE (game_id, game_name)
);

CREATE TABLE IF NOT EXISTS seasons (
    season_id int(11) AUTO_INCREMENT PRIMARY KEY,
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    name varchar(255),
    game_id int(11), -- represents which game this season belongs to.
    FOREIGN KEY (game_id) REFERENCES games(game_id)
);

CREATE TABLE IF NOT EXISTS series (
    series_id int(11) PRIMARY KEY,
    series_name varchar(255),
    description varchar(255),
    season_id int(11),
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    team_management_closing_time DATETIME, -- WTF are these? (related to minecraft team creation)
    team_consolidation_run DATETIME, -- What are those?
    series_end_time DATETIME,
    series_start_time DATETIME,
    FOREIGN KEY (season_id) REFERENCES seasons(season_id)
);

CREATE TABLE IF NOT EXISTS theater_chain (
    theater_chain_id int(11) PRIMARY KEY AUTO_INCREMENT,
    theater_chain_name varchar(255)
);

CREATE TABLE IF NOT EXISTS venues (
    venue_id int(11) PRIMARY KEY AUTO_INCREMENT,
    venue_name varchar(255),
    venue_owner varchar(255),
    address varchar(255),
    phone_number int(11),
    zip_code int(11),
    city_id int(11),
    state_id int(11),
    country_id int(11),
    google_map_link LONGTEXT,
    market varchar(150),
    timezone varchar(65),
    region varchar(10),
    theater_chain_id int(11),
    currency_accepted varchar(255),
    hyper_link varchar(255),
    FOREIGN KEY (city_id) REFERENCES cities(city_id),
    FOREIGN KEY (state_id) REFERENCES states(state_id),
    FOREIGN KEY (country_id) REFERENCES countries(country_id),
    FOREIGN KEY (theater_chain_id) REFERENCES theater_chain(theater_chain_id)
);

CREATE TABLE IF NOT EXISTS venue_contacts (
    venue_contact_id int(11) PRIMARY KEY AUTO_INCREMENT,
    venue_id int(11),
    role varchar(255),
    first_name varchar(255),
    last_name varchar(255),
    email varchar(255),
    phone_number varchar(100),
    send_theater_chain_report BOOLEAN NOT NULL DEFAULT 0,
    FOREIGN KEY (venue_id) REFERENCES venues(venue_id)
);

CREATE TABLE IF NOT EXISTS clubs ( -- can we use this table for all types of club? It's what I'm doing at the moment.
    club_id int(11) AUTO_INCREMENT PRIMARY KEY,
    club_name varchar(100),
    city_id int(11),
    state_id int(11),
    country_id int(11),
    division varchar(8),
    venue_id int(11), -- need to implement a venue table
    type varchar(500), -- indicates minecraft team/lol team. Is it neceessary?
    club_owner varchar(500),
    notes longtext,
    image varchar(100),
    short_name varchar(200), -- What is this?
    creation_date datetime DEFAULT CURRENT_TIMESTAMP, -- time at which this team was created
    club_image varchar(255),
    FOREIGN KEY (city_id) REFERENCES cities(city_id),
    FOREIGN KEY (state_id) REFERENCES states(state_id),
    FOREIGN KEY (country_id) REFERENCES countries(country_id),
    FOREIGN KEY (venue_id) REFERENCES venues(venue_id)
);

CREATE TABLE IF NOT EXISTS club_teams ( -- mostly just to ensure referential integrity. Note that the club teams do not contain a reference to users since teams are not always set. The user is related to teams in the club roster as it appears to be more rational to use the user as a condidate key for which team he/she belongs to than to have the team be the candidate key.
    team_id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    team_name varchar(255),
    club_id int(11),
    FOREIGN KEY (club_id) REFERENCES clubs(club_id)
);

CREATE TABLE IF NOT EXISTS club_roster (
    club_id int(11),
    user_id int(11),
    -- team_id int(11),
    active boolean, -- Keep track if the user is still part of the team. Need to determine if this column is necessary. Ultimately, if this takes up too much space we could simply drop users from this tables based on this boolean value.
    PRIMARY KEY (club_id, user_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (club_id) REFERENCES clubs(club_id)
    -- FOREIGN KEY (team_id) REFERENCES club_teams(team_id)
);

CREATE TABLE IF NOT EXISTS events (
    event_id int(11) AUTO_INCREMENT PRIMARY KEY,
    event_date DATETIME DEFAULT CURRENT_TIMESTAMP,
    venue_id int(11),
    name varchar(255),
    start_time time NOT NULL,
    end_time time NOT NULl,
    ticket_price decimal(9,2) NOT NULL,
    description varchar(500),
    vendor_id int(11), -- create a vendor table
    game_package_id int(11), -- what is game package supposed to be
    series_id int(11),
    buy_tickets_screen_special_instructions longtext,
    is_private_test_event BOOLEAN NOT NULL DEFAULT 0,
    include_in_json_data_feed BOOLEAN NOT NULL DEFAULT 0,
    date_voting_closes DATETIME,
    action_type_id int(11),
    FOREIGN KEY (series_id) REFERENCES series(series_id),
    FOREIGN KEY (venue_id) REFERENCES venues(venue_id)
);

CREATE TABLE IF NOT EXISTS matches (
    match_id int(11),
    match_name varchar(255),
    match_type varchar(255), -- double elimination, etc.
    game_code varchar(255), -- specific code given for the game (tournament code, server id, etc.) this is what allows people to join the game in question
    start_time DATETIME,
    event_id int(11),
    is_done BOOLEAN,
    winning_team int(11), -- This is important as teams don't stay the same
    losing_team int(11), -- also important as match participants can change team
    FOREIGN KEY (losing_team) REFERENCES club_teams(team_id),
    FOREIGN KEY (winning_team) REFERENCES club_teams(team_id),
    PRIMARY KEY (match_id),
    FOREIGN KEY (event_id) REFERENCES events(event_id)
);

CREATE TABLE IF NOT EXISTS match_participants ( --
    user_id int(11),
    match_id int(11),
    team_joined_as int(11), -- indicates which team the person joined as (keeps track of historical team participation)
    is_fill BOOLEAN DEFAULT 0, -- indicates whether or not this person is a fill in player (replacement player)
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (match_id) REFERENCES matches(match_id),
    FOREIGN KEY (team_joined_as) REFERENCES club_teams(team_id),
    PRIMARY KEY (user_id, match_id) -- the same person can't be in the same match twice
);

CREATE TABLE IF NOT EXISTS lol_match_stats ( -- maintains lol specific statistics
    user_id int(11),
    match_id int(11),
    longest_kill_streak int(11),
    quadra_kills int(11),
    penta_kills int(11),
    gold int(11),
    creeps int(11),
    update_time datetime,
    deaths int(11),
    double_kills int(11),
    kills int(11),
    triple_kills int(11),
    assists int(11),
    damage_dealt int(11),
    damage_taken int(11),
    lol_role varchar(255),
    win BOOLEAN,
    champion_id int(11),
    champion varchar(255),
    champion_key varchar(255),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (match_id) REFERENCES matches(match_id)
);

-- Note: a similar table will be created to track each game specific match result

CREATE TABLE IF NOT EXISTS ticketing ( -- Ticketing table indicates all necessary basic infomation about tickets. It reduces data reetnry from previous design.
    ticketing_id int(11) AUTO_INCREMENT PRIMARY KEY,
    ticketing_name varchar(255) NOT NULL DEFAULT "Standard Ticketing", -- In the case of multiple ticketing sales per event (i.e. golden ticket event or promor ticket events) this name can be changed.
    event_id int(11),
    max_tickets int(11),
    ticket_price decimal(9,2),
    sold_out BOOLEAN NOT NULL DEFAULT 0,
    FOREIGN KEY (event_id) REFERENCES events(event_id),
    CONSTRAINT ticket_name_req UNIQUE (ticketing_name, event_id) -- should not have the same name for a ticketing entry in the same event.
);

CREATE TABLE IF NOT EXISTS ticketing_promo_codes ( -- Tracks ticketing promotion codes. The actual logic for how these codes are used to provide discounts will be handled in the backend.
    ticketing_id int(11),
    promo_code varchar(255),
    FOREIGN KEY (ticketing_id) REFERENCES ticketing(ticketing_id),
    PRIMARY KEY (ticketing_id, promo_code)
);

CREATE TABLE IF NOT EXISTS tickets (
    -- created upon user completed purchase
    ticket_id int(11) AUTO_INCREMENT PRIMARY KEY,
    user_id int(11),
    ticketing_id int(11),
    redeem_key varchar(255) NOT NULL UNIQUE,
    status varchar(50),
    response_text varchar(255),
    affiliate_id varchar(128),
    comped_by varchar(255),
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    charge_id int(11),
    assigned_to varchar(255),
    date_assigned DATETIME,
    request_auto_team_assignment BOOLEAN DEFAULT 0,
    promo_code varchar(255),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (ticketing_id) REFERENCES ticketing(ticketing_id)
);

CREATE TABLE IF NOT EXISTS user_ticket_transactions (
    -- transaction stored upon ticket creation
    payment_info_id int(11),
    ticket_id int(11),
    discount decimal(9, 2) DEFAULT 0.00, -- use this to determine the final price
    paid BOOLEAN, -- indicate whether or not the user paid the amount
    disputed BOOLEAN,
    refunded BOOLEAN,
    amount_refunded decimal(9, 2),
    fee decimal(9, 2),
    reciept_sent BOOLEAN,
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    promo_contact_id int(11),
    currency varchar(10),
    PRIMARY KEY (payment_info_id, ticket_id),
    FOREIGN KEY (payment_info_id) REFERENCES user_payment_info(payment_info_id),
    FOREIGN KEY (ticket_id) REFERENCES tickets(ticket_id)
);

CREATE TABLE IF NOT EXISTS user_ticketing_cart (
    ticketing_id int(11),
    user_id int(11),
    quantity int(11),
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (ticketing_id) REFERENCES ticketing(event_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id)
);

-- This is meant to be the general market functionality

CREATE TABLE IF NOT EXISTS merchandise (
    merch_id int(11) PRIMARY KEY AUTO_INCREMENT,
    merch_type varchar(255), -- indicates the type of merchandise (shirt, cards, lighters, etc.)
    merch_name varchar(2000),
    description varchar(2000),
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    number_available int(11),
    size varchar(100),
    price decimal(9, 2),
    sold_out BOOLEAN DEFAULT 0
);

CREATE TABLE IF NOT EXISTS user_merch_transactions (
    payment_info_id int(11),
    merch_id int(11),
    merch_reciept int(11),
    discount decimal(9, 2) DEFAULT 0.00, -- use this to determine the final price
    paid BOOLEAN, -- indicate whether or not the user paid the amount
    disputed BOOLEAN,
    refunded BOOLEAN,
    amount_refunded decimal(9, 2),
    fee decimal(9, 2),
    reciept_sent BOOLEAN,
    currency varchar(50),
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (payment_info_id) REFERENCES user_payment_info(payment_info_id),
    FOREIGN KEY (merch_id) REFERENCES merchandise(merch_id)
);

CREATE TABLE IF NOT EXISTS user_merch_cart (
    user_id int(11),
    merch_id int(11),
    quantity int(11),
    date_created DATETIME DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (merch_id) REFERENCES merchandise(merch_id)
);

CREATE TABLE IF NOT EXISTS videos ( -- simple video hosting (not sure if this is something that is necessary)
    video_id int(11) PRIMARY KEY AUTO_INCREMENT,
    user_id int(11), -- indicates which user account created the video
    video_title varchar(255),
    video_desc varchar(2000),
    video_location varchar(1000), -- location of the video on the server (note that this is not a streamed element and so can be stored on the server to be sent client side when requested)
    validated BOOLEAN,
    FOREIGN KEY (user_id) REFERENCES users(user_id)
);


DROP DATABASE IF EXISTS nthgames2_auth;

CREATE DATABASE IF NOT EXISTS nthgames2_auth;

USE nthgames2_auth;

CREATE TABLE IF NOT EXISTS user_auth (
    user_id int(11) PRIMARY KEY AUTO_INCREMENT,
    open_id varchar(100) UNIQUE,
    password varchar(128) NOT NULL,
    last_login datetime,
    email varchar(255) NOT NULL UNIQUE,
    date_created datetime DEFAULT CURRENT_TIMESTAMP,
    is_verified BOOLEAN NOT NULL DEFAULT 0,
    email_token varchar(64), -- for user verification
    first_name varchar(255) NOT NULL,
    last_name varchar(255) NOT NULL
);



-- CREATE TABLE IF NOT EXSITS

-- note: use this query to get leaderboards
-- select summoner_id, COUNT(DISTINCT id) num_matches, SUM(CASE WHEN win = 1 THEN 1 ELSE 0 END) wins, SUM(CASE WHEN win = 0 THEN 1 ELSE 0 END) losses from lol_match_stats group by summoner_id order by wins desc;

-- DROP DATABASE IF EXISTS nthgames2;
